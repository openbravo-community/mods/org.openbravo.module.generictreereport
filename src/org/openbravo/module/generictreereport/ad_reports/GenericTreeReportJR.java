package org.openbravo.module.generictreereport.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.module.generictreereport.dao.GenericTreeReportDao;
import org.openbravo.xmlEngine.XmlDocument;

public class GenericTreeReportJR extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private static GenericTreeReportDao dao;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strReportName = vars.getGlobalVariable("reportName",
        "GenericTreeReportJR|GTR_REPORT_NAME", "");
    if (vars.commandIn("DEFAULT")) {
      String strReportID = vars.getGlobalVariable("inpgtrTreeReportId",
          "GenericTreeReportJR|GTR_TREE_REPORT_ID", "");
      printPageDataSheet(response, vars, strReportID);
    } else if (vars.commandIn("PRINT_PDF")) {
      String strReportID = vars.getGlobalVariable("inpgtrTreeReportId",
          "GenericTreeReportJR|GTR_TREE_REPORT_ID", "");
      printPageDataPDF(request, response, vars, strReportID, strReportName);
    } else
      pageError(response);
  }

  private void printPageDataPDF(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strReportID, String strReportName) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    dao = new GenericTreeReportDao();
    FieldProvider[] data = null;
    log4j.debug("Output: Generic Tree View Report");
    try {
      data = dao.getSelect(strReportID);
    } catch (Exception e) {
      throw new ServletException(Utility.messageBD(this, "GTR_INCORRECT_PARAMETER", vars
          .getLanguage()));
    }
    if (data == null || data.length == 0) {
      advisePopUp(request, response, "WARNING", Utility.messageBD(this, "NoDataFound", vars
          .getLanguage()));
    } else {
      String strLanguage = vars.getLanguage();
      String strMainReportName = "@basedesign@/org/openbravo/module/generictreereport/ad_reports/GenericTreeReportPDF.jrxml";
      HashMap<String, Object> parameters = new HashMap<String, Object>();
      parameters.put("Title", strReportName);
      parameters.put("report_name", Utility.messageBD(this, strReportName, strLanguage));
      parameters.put("USER_ORG", Utility.getContext(this, vars, "#AccessibleOrgTree",
          "ReportBankJR"));
      parameters.put("USER_CLIENT", Utility.getContext(this, vars, "#User_Client", "ReportBankJR"));
      renderJR(vars, response, strMainReportName, "pdf", parameters, data, null);
    }
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strReportID) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    String strMessage = "";

    XmlDocument xmlDocument = null;
    xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/generictreereport/ad_reports/GenericTreeReportJR")
        .createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "GenericTreeReportJR", false, "", "",
        "imprimir();return false;", false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "org.openbravo.module.generictreereport.ad_reports.GenericTreeReportJR");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "GenericTreeReportJR.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "GenericTreeReportJR.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    OBError myMessage = vars.getMessage("GenericTreeReportJR");
    vars.removeMessage("GenericTreeReportJR");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
        + "');"));
    xmlDocument.setParameter("treeReportId", strReportID);

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "GTR_TREE_REPORT_ID",
          "C613E22897BA4DE2882DBE9072D595E0", "", Utility.getContext(this, vars, "#User_Org",
              "GenericTreeReportJR"), Utility.getContext(this, vars, "#User_Client",
              "GenericTreeReportJR"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "GenericTreeReportJR",
          strReportID);
      xmlDocument.setData("ID_reportNameDropdown", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    out.println(xmlDocument.print());
    out.close();
  }

  public String getServletInfo() {
    return "Servlet GenericTreeReportJR.";
  }
}
