package org.openbravo.module.generictreereport.ad_reports;

import org.apache.log4j.Logger;
import org.openbravo.data.FieldProvider;

@SuppressWarnings("serial")
public class TreeReportData implements FieldProvider {
  static Logger log4j = Logger.getLogger(TreeReportData.class);
  private String InitRecordNumber = "0";
  public String adClientId;
  public String adOrgId;
  public String name;
  public String level;
  public String primaryId;
  public String isSummary;
  public String parentId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("NAME"))
      return name;
    else if (fieldName.equalsIgnoreCase("PRIMARYID"))
      return primaryId;
    else if (fieldName.equalsIgnoreCase("ISSUMMARY"))
      return isSummary;
    else if (fieldName.equalsIgnoreCase("PARENTID"))
      return parentId;
    else {
      log4j.debug("Field does not exist: " + fieldName);
      return null;
    }
  }

}
