package org.openbravo.module.generictreereport.dao;

/*
 ************************************************************************************
 * Copyright (C) 2010-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Public License Version 1.1
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.module.generictreereport.GenericTreeReport;
import org.openbravo.module.generictreereport.ad_reports.TreeReportData;

public class GenericTreeReportDao {
  private List<TreeReportData> objectTreeReportData = new ArrayList<TreeReportData>();

  public FieldProvider[] getSelect(String strReportID) throws ServletException {
    try {
      OBContext.setAdminMode(true);
      TreeReportData objTreeReportData = null;
      String compareField;
      GenericTreeReport gtReport = OBProvider.getInstance().get(GenericTreeReport.class);
      final OBCriteria<GenericTreeReport> obc = OBDal.getInstance().createCriteria(
          GenericTreeReport.class);
      obc.add(Restrictions.eq("id", strReportID));
      final List<GenericTreeReport> gtReportList = obc.list();
      gtReport = gtReportList.get(0);
      String rootElement = (gtReport.getRootElement() != null) ? gtReport.getRootElement() : "";

      StringBuffer queryStr = new StringBuffer().append("select ")
          .append(gtReport.getPrimaryidColName().getDBColumnName()).append(" as primaryId, ")
          .append(gtReport.getIssummaryColName().getDBColumnName()).append(" as isSummary, ")
          .append(gtReport.getParentidColName().getDBColumnName())
          .append(" as parentId, name from ").append(gtReport.getTable().getDBTableName());

      if (gtReport.getSequenceNumber() != null
          && !gtReport.getSequenceNumber().getDBColumnName().equalsIgnoreCase("")) {
        queryStr.append(" order by ");
        queryStr.append(gtReport.getSequenceNumber().getDBColumnName());
      }

      Session session = SessionHandler.getInstance().getSession();
      @SuppressWarnings("rawtypes")
      final NativeQuery sqlQuery = session.createNativeQuery(queryStr.toString());
      List<TreeReportData> objTreeReportDataList = new ArrayList<TreeReportData>();

      for (final Object record : sqlQuery.list()) {
        final Object[] value = (Object[]) record;
        objTreeReportData = new TreeReportData();
        objTreeReportData.primaryId = (value[0] == null) ? "" : value[0].toString();
        objTreeReportData.isSummary = (value[1] == null) ? "" : value[1].toString();
        objTreeReportData.parentId = (value[2] == null) ? "" : value[2].toString();
        objTreeReportData.name = (value[3] == null) ? "" : value[3].toString();
        objTreeReportDataList.add(objTreeReportData);
      }

      Iterator<TreeReportData> itr = objTreeReportDataList.iterator();
      while (itr.hasNext()) {
        objTreeReportData = itr.next();
        compareField = rootElement.isEmpty() ? objTreeReportData.parentId
            : objTreeReportData.primaryId;
        if (compareField.equalsIgnoreCase(rootElement)) {
          objTreeReportData.name = fillString(1) + "-" + objTreeReportData.name;
          objectTreeReportData.add(objTreeReportData);
          arrangeData(objTreeReportDataList, objTreeReportData.primaryId, 1);
        }
      }

      TreeReportData objTreeRptData[] = new TreeReportData[objectTreeReportData.size()];
      objTreeRptData = objectTreeReportData.toArray(objTreeRptData);
      return objTreeRptData;
    } catch (Exception ex) {
      throw new ServletException(ex);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void arrangeData(List<TreeReportData> arrayTreeReportData, String primaryId, int level)
      throws Exception {
    TreeReportData objTreeReportData;
    Iterator<TreeReportData> itr = arrayTreeReportData.iterator();
    while (itr.hasNext()) {
      objTreeReportData = itr.next();
      if (objTreeReportData.parentId.equalsIgnoreCase(primaryId)) {
        objTreeReportData.name = fillString(level * 8) + "-" + objTreeReportData.name;
        objectTreeReportData.add(objTreeReportData);
        arrangeData(arrayTreeReportData, objTreeReportData.primaryId, level + 1);
      }
    }
  }

  private String fillString(int fillCount) {
    char[] s = new char[fillCount];
    Arrays.fill(s, ' ');
    return new String(s);
  }
}